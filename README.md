# Hpc Parallel Exercises

Exercises for the parallel computing lectures

## hello

Compile with  
```mpiifort hello.f90 -o hello```

Run with  
```mpirun -n 3 ./hello```

## pingpong

Compile with  
```mpiifort pingpong.f90 -o pingpong```

Run with  
```mpirun -n 2 ./pingpong```
