!*****************************************************************************************
!> author: Fabrice Roy
!  license: GPL V3
!
!  Distributed max toy program
!
! Contact: fabrice.roy@observatoiredeparis.psl.eu
!
! Tokenring is part of the tutorials of the HPC Master Class of the Graduate program AStroParis: Astrophysics and Space
!
! Tokenring is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Tokenring is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with these tutorials. If not, see <http://www.gnu.org/licenses/>.

!*****************************************************************************************
!> author: Fabrice Roy
!  date: 2021/10/25
program distributed_max

  use iso_fortran_env, only : ERROR_UNIT, OUTPUT_UNIT
  use mpi
  
  implicit none

  integer :: alloc_stat
  integer :: comm_rank
  integer :: comm_size
  integer :: dest
  integer, allocatable, dimension(:) :: displacements
  character(100) :: error_message
  integer, allocatable, dimension(:) :: global_array
  integer :: global_max
  integer, parameter :: global_size = 20
  integer :: i
  integer :: ierr
  integer :: i_round
  integer, allocatable, dimension(:) :: local_array
  integer :: local_max
  integer :: local_size
  integer :: partner_max
  integer :: prov
  real :: rand_val
  integer, parameter :: range = 100000000
  integer :: rounds_number
  integer, allocatable, dimension(:) :: send_counts
  integer, dimension(MPI_STATUS_SIZE) :: status

  call mpi_init(ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, comm_rank, ierr)
  call mpi_comm_size(MPI_COMM_WORLD, comm_size, ierr)

  ! rank 0 creates the array and initializes it with random values
  if ( comm_rank == 0 ) then
    allocate(global_array(global_size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
      write(ERROR_UNIT,'(a)') 'allocation error for global_array'
      call mpi_abort(MPI_COMM_WORLD, 1, ierr)
    end if
    ! random_number returns a real between 0 and 1
    do i = 1, global_size
      call random_number(rand_val)
      ! int returns the integer part
      global_array(i) = int(rand_val*range)
    end do
  end if

! the code between #ifdef DEBUG and #endif is compiled only if the preprocessor instructions are used 
! (source code extension is capital eg .F90 for instance or compilation flag -cpp) 
! and if the compilation flag -DDEBUG is used
#ifdef DEBUG
  if(comm_rank == 0) write(OUTPUT_UNIT,*) 'Global: ', global_array
#endif

  ! we assume that the size of the global array is greater than the number of processes
  ! if the size of the array is a multiple of the number of processes
  if(mod(global_size, comm_size) == 0) then
    local_size = global_size / comm_size
    ! allocation of the local array
    allocate(local_array(local_size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
      write(ERROR_UNIT,'(a)') 'allocation error for local_array'
      call mpi_abort(MPI_COMM_WORLD, 1, ierr)
    end if
    ! scatter from 0 to everyone
    call mpi_scatter(global_array,local_size,MPI_INTEGER,local_array,local_size,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  else ! if the size of the array is not a multiple of the number of processes
    ! we allocate arrays for the count of elements sent to each process and the displacements to the first element sent to each process
    allocate(send_counts(comm_size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
      write(ERROR_UNIT,'(a)') 'allocation error for send_counts'
      call mpi_abort(MPI_COMM_WORLD, 1, ierr)
    end if
    allocate(displacements(comm_size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
      write(ERROR_UNIT,'(a)') 'allocation error for displacements'
      call mpi_abort(MPI_COMM_WORLD, 1, ierr)
    end if
    ! if you divide 2 integers and affect the result to an integer, you get the integer part
    local_size = global_size / comm_size
    displacements(1) = 0
    send_counts = local_size
    do i = 1, mod(global_size,comm_size) 
      send_counts(i) = send_counts(i) + 1
    end do
    do i = 2, comm_size
      displacements(i) = displacements(i-1) + send_counts(i-1)
    end do
    local_size = send_counts(comm_rank+1)
    ! allocation of the local array
    allocate(local_array(local_size), stat=alloc_stat, errmsg=error_message)
    if(alloc_stat /= 0) then
      write(ERROR_UNIT,'(a)') 'allocation error for local_array'
      call mpi_abort(MPI_COMM_WORLD, 1, ierr)
    end if
    ! scatterv from 0 to everyone
    call mpi_scatterv(global_array,send_counts,displacements,MPI_INTEGER,local_array,&
    local_size,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
  end if

  ! get the local max with intrisic fortran function maxval
  local_max = maxval(local_array)

#ifdef DEBUG
  write(OUTPUT_UNIT,*) 'process',comm_rank, ' ; max = ', maxval(local_array), ' ; local: ', local_array
#endif

! if the code is not compiled with -DCOLLECTIVE then we use the tournament
#ifndef COLLECTIVE
  ! number of rounds in the tournament using fortran intrinsic function ceiling wich returns the least integer greater than its argument
  rounds_number = ceiling(log(real(comm_size)) / log(2.))

  ! loop over the rounds
  do i_round = 1, rounds_number
    partner_max = 0
    ! dest and prov initialized with MPI_PROC_NULL which means "nowhere"
    dest = MPI_PROC_NULL
    prov = MPI_PROC_NULL
    ! if my rank is a multiple of 2^(i_round-1) I may have to communicate
    if( mod(comm_rank,2**(i_round-1)) == 0 ) then 
      if( mod(comm_rank,2**i_round) ==0 ) then
        prov = comm_rank + 2**(i_round-1)
      else
        dest = comm_rank - 2**(i_round-1) 
      end if
    end if
    ! prov cannot be equal or greater than the number of process
    if (prov >= comm_size) prov = MPI_PROC_NULL
    ! everyone sends and receives but only those whoe dest or prov is not MPI_PROC_NULL will actually do something
    call mpi_send(local_max,1,MPI_INTEGER,dest,0,MPI_COMM_WORLD,ierr)
    call mpi_recv(partner_max,1,MPI_INTEGER,prov,0,MPI_COMM_WORLD,status,ierr)
    ! local_max is compared to partner_max, the max received 
    local_max = max(local_max,partner_max)
  end do
  if(comm_rank == 0) global_max = local_max
  ! else, if -DCOLLECTIVE is used, we use the collective communication
#else 
  call mpi_reduce(local_max,global_max,1,MPI_INTEGER,MPI_MAX,0,MPI_COMM_WORLD,ierr)
#endif

  ! in the end process 0 has the global max
if(comm_rank == 0) write(OUTPUT_UNIT,*) 'global max = ', global_max

#ifdef DEBUG
  ! check if the global max found with the tournament is the same as the one found with maxval
  if(comm_rank == 0) write(OUTPUT_UNIT,*) 'max of global array = ',maxval(global_array)
#endif

  call mpi_finalize(ierr)

end program distributed_max